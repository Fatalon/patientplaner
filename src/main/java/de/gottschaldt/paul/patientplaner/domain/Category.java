package de.gottschaldt.paul.patientplaner.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@SuppressWarnings("unused")
@Entity
public class Category {
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Integer id;

	private String name;

	@Column(name = "start_of_timeslot")
	private LocalTime startOfTimeslot;
	@Column(name = "end_of_timeslot")
	private LocalTime endOfTimeslot;

	@Column(name = "timeslot_set")
	private boolean timeslotSet;

	@Column(name = "next_possible_day")
	private LocalDate nextPossibleDay;

	protected Category() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalTime getStartOfTimeslot() {
		return startOfTimeslot;
	}

	public void setStartOfTimeslot(LocalTime startOfTimeslot) {
		this.startOfTimeslot = startOfTimeslot;
	}

	public LocalTime getEndOfTimeslot() {
		return endOfTimeslot;
	}

	public void setEndOfTimeslot(LocalTime endOfTimeslot) {
		this.endOfTimeslot = endOfTimeslot;
	}

	public boolean isTimeslotSet() {
		return timeslotSet;
	}

	public void setTimeslotSet(boolean timeslotSet) {
		this.timeslotSet = timeslotSet;
	}

	public LocalDate getNextPossibleDay() {
		return nextPossibleDay;
	}

	public void setNextPossibleDay(LocalDate nextPossibleDay) {
		this.nextPossibleDay = nextPossibleDay;
	}
}
