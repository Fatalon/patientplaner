package de.gottschaldt.paul.patientplaner.repository;

import de.gottschaldt.paul.patientplaner.domain.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Integer> {
}
