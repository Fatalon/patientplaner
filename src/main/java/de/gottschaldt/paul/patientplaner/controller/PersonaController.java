package de.gottschaldt.paul.patientplaner.controller;

import de.gottschaldt.paul.patientplaner.domain.Category;
import de.gottschaldt.paul.patientplaner.repository.CategoryRepository;
import org.springframework.web.bind.annotation.*;


@RestController
//@RequestMapping("/api/persons")
public class PersonaController {

	private CategoryRepository categoryRepository;

	public PersonaController(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	@GetMapping("/categories")
	public Iterable<Category> getAllCategories() {
		return categoryRepository.findAll();
	}

	@PostMapping("/categories")
	Category newCategory(@RequestBody Category newCategory) {
		return categoryRepository.save(newCategory);
	}
}
