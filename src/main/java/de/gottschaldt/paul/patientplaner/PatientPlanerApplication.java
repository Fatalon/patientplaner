package de.gottschaldt.paul.patientplaner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PatientPlanerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PatientPlanerApplication.class, args);
	}

}
